package com.example.simplegame;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class SimpleGameEngine extends Activity
    {
        // gameView will be the view of the game
        // It will also hold the logic of the game
        // and respond to screen touches as well
        GameView gameView;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Initialize gameView and set it as the view
            gameView = new GameView(this);
            setContentView(gameView);
        }
        // GameView class will go here
        // More SimpleGameEngine methods will go here

        @Override
        protected void onResume() {
            super.onResume();

            // Tell the gameView resume method to execute
            gameView.resume();
        }
        // This method executes when the player quits the game
        @Override
        protected void onPause() {
            super.onPause();

            // Tell the gameView pause method to execute
            gameView.pause();
        }
    }