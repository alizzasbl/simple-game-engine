package com.example.simplegame;

import android.graphics.BitmapFactory;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

// Notice we implement runnable so we have
// A thread and can override the run method.
class GameView extends SurfaceView implements Runnable
    {

        // This is our thread
        Thread gameThread = null;

        // This is new. We need a SurfaceHolder
        // When we use Paint and Canvas in a thread
        // We will see it in action in the draw method soon.
        SurfaceHolder ourHolder;

        // A boolean which we will set and unset
        // when the game is running- or not.
        volatile boolean playing;

        // A Canvas and a Paint object
        Canvas canvas;
        Paint paint;

        // This variable tracks the game frame rate
        long fps;

        // This is used to help calculate the fps
        private long timeThisFrame;

        // Declare an object of type Bitmap
        Bitmap bitmapBob;

        // Bob starts off not moving
        boolean isMoving = false;

        // He can walk at 150 pixels per second
        float walkSpeedPerSecond = 500;

        // He starts 10 pixels from the left
        float bobXPosition = 10;
        int width;

        public GameView(Context context) {

            // The next line of code asks the
            // SurfaceView class to set up our object.
            // How kind
            super(context);

            // Initialize ourHolder and paint objects
            ourHolder = getHolder();
            paint = new Paint();

            // Load Bob from his .png file

            bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.tonyy);
            Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
            width = display.getWidth() - bitmapBob.getWidth();

        }

        @Override
        public void run() {
            while (playing) {
                // Capture the current time in milliseconds in startFrameTime
                long startFrameTime = System.currentTimeMillis();

                // Update the frame
                update();

                // Draw the frame
                draw();

                // Calculate the fps this frame
                // We can then use the result to
                // time animations and more.
                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame > 0) {
                    fps = 1000 / timeThisFrame;
                }
            }
        }

        private void draw() {
            // Make sure our drawing surface is valid or we crash
            if (ourHolder.getSurface().isValid()) {

            // Lock the canvas ready to draw
            // Make the drawing surface our canvas object
                canvas = ourHolder.lockCanvas();

            // Draw the background color
                canvas.drawColor(Color.argb(255, 26, 128, 182));

            // Choose the brush color for drawing
                paint.setColor(Color.argb(255, 249, 129, 0));

            // Make the text a bit bigger
                paint.setTextSize(45);

            // Display the current fps on the screen
                canvas.drawText("FPS:" + fps, 20, 40, paint);

            // Draw tony at tonyXPosition, 200 pixels
                canvas.drawBitmap(bitmapBob, bobXPosition, 200, paint);
                //canvas.drawBitmap(bitmapBob, 10, 10,  paint);

            // Draw everything to the screen
            // and unlock the drawing surface
                ourHolder.unlockCanvasAndPost(canvas);
            }
        }

        public void update() {
            // If bob is moving (the player is touching the screen)
            // then move him to the right based on his target speed and the current fps.
            if (isMoving) {
                bobXPosition += walkSpeedPerSecond / fps;


                if (bobXPosition > width || bobXPosition < 0) {
                    walkSpeedPerSecond = -walkSpeedPerSecond;
                }


            }
        }

        public void pause() {
            playing = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }
        }

        // If SimpleGameEngine Activity is started theb
        // start our thread.
        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

                // Player has touched the screen
                case MotionEvent.ACTION_DOWN:

                    // Set isMoving so Bob is moved in the update method

                    isMoving = true;
                    break;

                // Player has removed finger from screen
                case MotionEvent.ACTION_UP:

                // Set isMoving so Bob does not move

                    isMoving = false;
                    break;
            }
            return true;
        }
    }